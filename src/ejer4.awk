#for run:
#awk -f ejer4.awk ../data/names.txt > distr.txt

BEGIN{
	print "*Distribution by hours*";
}

function complete(s){ 
	if (s >= 10) return s
	return ("0" s) # Or return sprintf("0%s", s)
}


{
	if($5 != ""){
		arr[complete((int($5)))]++;
	}
}



END{
	n = asorti(arr, indexes);
	for(i=1; i<=n;i++){
		k = indexes[i];
		printf "Hours=%5.1d Persons=%3d\n",k,arr[k];
		#Como lo que tengo en el vector es un string,
		#para colocarlo de forma numero tengo que usar %3d o %5.1.
	}
}