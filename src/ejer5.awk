#awk -f ejer5.awk ../data/names.txt > maysal.txt

BEGIN{
	print "* Mayor salary *";
	mayor = 0;
	nombre;
}
{	
	salario = $4 * $5;
	if (mayor < salario){
		mayor = salario;
		nombre = $1;
	}
}
END{
	printf "Salary =%5.2f Name=%s \n",mayor,nombre;		
}