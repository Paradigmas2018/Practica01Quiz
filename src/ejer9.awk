#Ejercicio 9
#Mayor Diferencia de salarios (hombre y mujer)
BEGIN{
men_mujer=0;
men_hombre=0;
cont_mujer=0;
cont_hombre=0;
}

$3~/female/{
cont_mujer++;
salario=$4*$5;
  if (cont_mujer==1) {
  men_mujer=salario;
  may_mujer=salario;
 }
 else{
(salario>may_mujer)? may_mujer=salario: (salario<men_mujer) ?men_mujer=salario: "";
 }

}

$3~/^male/{
cont_hombre++;
salario=$4*$5;
  if (cont_hombre==1) {
  men_hombre=salario;
  may_hombre=salario;
 }
 else{
 (salario>may_hombre) ? may_hombre=salario :(salario<men_hombre)? men_hombre=salario:"";
 }

}
END{
printf "Hombre - Salario mayor %d, Salario menor %d, diferencia %d \n",may_hombre, men_hombre, may_hombre-men_hombre;
printf "Mujer - Salario mayor %d, Salario menor %d, diferencia %d \n",may_mujer, men_mujer, may_mujer-men_mujer;
}