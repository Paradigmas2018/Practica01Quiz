#for run # awk -f ejer2.awk ../data/names.txt > malesalary.txt


BEGIN{
	salario = 0;
	contador = 0;

}

$3~/^male/{
	salario += $4*$5;
	print $2" "$1" "$4*$5;	
	contador++;
	
}

END{
	print "Total:";
	print "---------> Hombres: "contador;
	print "---------> Salarios: "salario;
	print "---------> Promedio: "(salario/contador);
	

}