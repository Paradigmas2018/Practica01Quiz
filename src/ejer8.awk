#awk -f ejer8.awk ../data/names.txt > sortFemaleMale.html

function printArrayHTML(arr){
	f = asorti(arr, indexes);
	for (j = 1; j<=f; j++){
		k = indexes[j];
		print "<tr>"
		print 	"<th scope=\"row\" style=\"text-align:center\">"j"</th>";
		printf 	"<td style=\"text-align:center\">%s</td>\n", arr[k];	
		printf 	"<td style=\"text-align:center\">%5.2f</td>\n",k;	
		print "</tr>";
	}
}

BEGIN{
	salario = 0;
}

function complete(salario){
	return (salario < 10) ? ("00"salario):(salario<100)? ("0"salario) : salario;
}

$3 ~ /^female/{
	salario = complete(($4)*int($5));
	arrf[salario] = $2" "$1;
}

$3 ~ /^male/{
	salario = complete(($4)*int($5));
	arrm[salario] = $2" "$1;
}


END{

	print "<html>"
    print "<head>"
    print 	"<title>Tabla</title>"
    print 	"<meta charset=\"UTF-8\">"
    print 	"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">"
    print	"<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\"integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">";
	print "</head>"
    print "<body>"
    print    "<div id=\"wrapper\">"
	print		"<table class=\"table table-striped\" style=\"width:50%\" align=\"center\" >"
	print		"<thead>"
	print		"<tr>"
    print  			"<th scope=\"col\" style=\"text-align:center\">Num</th>";
    print           "<th scope=\"col\" style=\"text-align:center\">Name</th>";
    print           "<th scope=\"col\" style=\"text-align:center\">Salary</th>";
    print		"</tr>"
    print 		"</thead>";
    print 		"<tbody>"
	print 		"<tr><th></th><th style=\"text-align:center\">Mujeres</th><th></th></tr>"
	printArrayHTML(arrf);
	print "<tr><th></th><th style=\"text-align:center\">Hombres</th><th></th></tr>"
	printArrayHTML(arrm);
	print 		"</tbody>"
	print		"</table>"	
	print 	 "</div>"
    print "</body>"
	print "</html>"

}