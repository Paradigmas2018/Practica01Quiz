
#for run # awk -f ejer1.awk ../data/names.txt > femalesalary.txt


BEGIN{
	OFS = " "
	salario = 0;
	contador = 0;

}

/female/{
	salario += $4*$5;
	print $2" "$1" "$4*$5;
	contador++;
}

END{
	print "Total:";
	print "---------> Mujeres: "contador;
	print "---------> Salarios: "salario;
	print "---------> Promedio: "(salario/contador);
	

}