#awk -f ejer6.awk ../data/names.txt > salaries.txt

BEGIN{
	print "* Mayor salaries *";
	mayorMujer = 0;
	mayorHombre = 0;
	nameMujer = "";
	nameHombre = "";
}

$3 ~ /^female/{
	if(($4*$5) > mayorMujer){
		mayorMujer = $4*$5;
		nameMujer = $1;
	}
}


$3 ~ /^male/{
		if(($4*$5) > mayorHombre){
		mayorHombre = $4*$5;
		nameHombre = $1;
	}
}


END{
	printf "Name ----> %s ---> Salary:%5.2f (Female)\n", nameMujer,mayorMujer;
	printf "Name ----> %s ---> Salary:%5.2f (Male)\n", nameHombre, mayorHombre;
	
		
}