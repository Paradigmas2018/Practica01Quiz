#
# Calcula la distribución de personas segun horas trabajadas. Se asume máximo 99
# loriacarlos@gmail.com
# EIF400 II-2018 
BEGIN{
    print "*** Distribution by Hours ***"
	
}
function complete(s){ # Añade un cero si corresponde para que el sort funcione bien
	if (s >= 10) return s
	return ("0" s) # Or return sprintf("0%s", s)
}
# Regla para todos los records (lines)
{
  if ($5 != "")
	dist[complete(int($5))]++
	
}
END{
	
	n = asorti(dist, indexes) # Asume que las keys son strings!
	for (i = 1; i <= n; i++){
		k  = indexes[i]
		printf "Hours=%3d Persons=%3d\n", k, dist[k]
	}
}