#awk -f ejer7.awk ../data/names.txt > sortFemaleMale.txt

function printArray(arr){
	f = asorti(arr, indexes);
	for (j = 1; j<=f; j++){
		k = indexes[j];
		printf("%s %5.2f\n", arr[k], k);
	}
}

BEGIN{
	print "* Sorting Data *";
	salario = 0;
}

function complete(salario){
	return (salario < 10) ? ("00"salario):(salario<100)? ("0"salario) : salario;
}

$3 ~ /^female/{
	salario = complete(($4)*int($5));
	arrf[salario] = $2" "$1;
}

$3 ~ /^male/{
	salario = complete(($4)*int($5));
	arrm[salario] = $2" "$1;
}


END{
	print "Lista de mujeres por salario"
	printArray(arrf);
	print "Lista de hombres por salario";
	printArray(arrm);
	print "Done!"
}