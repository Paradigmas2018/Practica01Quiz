
echo "*** Ejercicios ***"
#
echo "*** Ejercicio 1 ***"
awk -f src/ejer1.awk data/names.txt > salidas/salida1.txt
echo "*** Ejercicio 2 ***"
gawk -f src/ejer2.awk  data/names.txt > salidas/salida2.txt
echo "*** Ejercicio 3 ***"
gawk -f src/ejer3.awk  data/names.txt > salidas/salida3.txt
echo "*** Ejercicio 4 ***"
gawk -f src/ejer4.awk  data/names.txt > salidas/salida4.txt
echo "*** Ejercicio 5 ***"
gawk -f src/ejer5.awk  data/names.txt > salidas/salida5.txt
echo "*** Ejercicio 6 ***"
gawk -f src/ejer6.awk  data/names.txt > salidas/salida6.txt
echo "*** Ejercicio 7 ***"
gawk -f src/ejer7.awk  data/names.txt > salidas/salida7.txt
echo "*** Ejercicio 8 ***"
gawk -f src/ejer8.awk data/names.txt > salidas/salida8.html
echo "*** Ejercicio 9 ***"
gawk -f src/ejer9.awk data/names.txt > salidas/salida9.txt